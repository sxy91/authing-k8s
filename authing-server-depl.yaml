apiVersion: apps/v1
kind: Deployment
metadata:
  name: authing-server
  namespace: authing
spec:
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 1
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: authing-server
      app.kubernetes.io/part-of: authing-server
  template:
    metadata:
      labels:
        app.kubernetes.io/name: authing-server
        app.kubernetes.io/part-of: authing-server
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - authing-server
                topologyKey: kubernetes.io/hostname
              weight: 100
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - authing-server
                topologyKey: failure-domain.beta.kubernetes.io/zone
              weight: 90
      imagePullSecrets:
        - name: regsecret
      terminationGracePeriodSeconds: 120
      volumes:
        - name: authing-server-config
          configMap:
            name: authing-server-config
        - name: authing-server-logstash-config
          configMap:
            name: authing-server-logstash-config
        - name: authing-server-logs
          emptyDir: {}
        - name: timezone
          hostPath:
            path: /usr/share/zoneinfo/Asia/Shanghai
      initContainers:
        - name: init-elasticsearch
          image: registry.cn-beijing.aliyuncs.com/authing-public/authing-server-public:curl-7.68.0
          command:
            [
              "sh",
              "-c",
              "until curl elasticsearch.authing:9200; do echo waiting for elasticsearch; sleep 1; done;",
            ]
          securityContext:
            privileged: true
        - name: init-migration
          image: registry.cn-beijing.aliyuncs.com/authing-public/authing-server-public:authing-2.40.67
          args:
            [
              "yarn",
              "migration:run",
            ]
          volumeMounts:
            - name: authing-server-config
              mountPath: /etc/authing
              readOnly: true
          securityContext:
            privileged: true
      containers:
        - name: authing-server
          image: registry.cn-beijing.aliyuncs.com/authing-public/authing-server-public:authing-2.40.67
          ports:
            - containerPort: 3000
              protocol: TCP
            - containerPort: 1389
              protocol: TCP
          volumeMounts:
            - name: authing-server-config
              mountPath: /etc/authing
              readOnly: true
            - name: authing-server-logs
              mountPath: /var/log/authing-server
            - name: timezone
              mountPath: /etc/localtime
          resources:
            limits:
              cpu: 500m
              memory: 2048Mi
            requests:
              cpu: 250m
              memory: 1024Mi
          lifecycle:
            preStop:
              exec:
                command:
                  - /wait-shutdown
          livenessProbe:
            failureThreshold: 10
            httpGet:
              path: /healthz
              port: 3000
              scheme: HTTP
            initialDelaySeconds: 60
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 50
          readinessProbe:
            failureThreshold: 10
            httpGet:
              path: /healthz
              port: 3000
              scheme: HTTP
            initialDelaySeconds: 60
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 50
          startupProbe:
            failureThreshold: 30
            httpGet:
              path: /healthz
              port: 3000
            periodSeconds: 10
        - name: authing-server-logstash
          image: docker.elastic.co/logstash/logstash:6.8.22
          volumeMounts:
            - name: authing-server-logs
              mountPath: /var/log/authing-server
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-access.conf
              subPath: logstash-access.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-audit.conf
              subPath: logstash-audit.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-error.conf
              subPath: logstash-error.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-user-action.conf
              subPath: logstash-user-action.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-metrics.conf
              subPath: logstash-metrics.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-webhook.conf
              subPath: logstash-webhook.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-pipeline.conf
              subPath: logstash-pipeline.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-custom-database-script-execution.conf
              subPath: logstash-custom-database-script-execution.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/config/logstash.yml
              subPath: logstash.yml
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash.conf
              subPath: logstash.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/config/pipelines.yml
              subPath: pipelines.yml
